package com.vicchern;

class DivisionResult {
    private Calculator calculator = new Calculator();
    private StringBuilder result = calculator.getResult();
    private StringBuilder reminder = calculator.getReminder();
    private StringBuilder quotient = calculator.getQuotient();

    String receiveReminderWithRightAmountOfSpacesInTheString(int counterOfTabs, String reminderNumberWithTabs, int multiplyResult) {
        reminderNumberWithTabs = String.format(String.format("%%%ds", counterOfTabs + 2), "_" + reminderNumberWithTabs);
        result.append(reminderNumberWithTabs).append("\n");

        String multiply = String.format(String.format("%%%dd", counterOfTabs + 2), multiplyResult);
        result.append(multiply).append("\n");
        return reminderNumberWithTabs;
    }

    int receiveReminderNumber(int divisor, int counterOfTabs) {
        int reminderNumber = Integer.parseInt(reminder.toString());
        if (reminderNumber >= divisor) {
            int multiplyResult = reminderNumber / divisor * divisor;
            int mod = reminderNumber % divisor;

            String reminderNumberToString = String.valueOf(reminderNumber);
            int tab = receiveReminderWithRightAmountOfSpacesInTheString(counterOfTabs, reminderNumberToString, multiplyResult).length() -
                    calculator.lengthOfStringArrayConvertedFromCurrentDividend(multiplyResult);

            result.append(new Formatter().makeDivider(reminderNumber, tab)).append("\n");

            quotient.append(reminderNumber / divisor);

            reminder.replace(0, reminder.length(), Integer.toString(mod));
            reminderNumber = Integer.parseInt(reminder.toString());

        } else if (counterOfTabs >= calculator.lengthOfStringArrayConvertedFromCurrentDividend(divisor)) {
            quotient.append(0);
        }
        return reminderNumber;
    }
}






package com.vicchern;

class Formatter {
    private Calculator calculator = new Calculator();

    String makeDivider(Integer reminderNumber, Integer tab) {
        return assemblyString(tab, ' ') +
                assemblyString(new Calculator().lengthOfStringArrayConvertedFromCurrentDividend(reminderNumber), '-');
    }

    private String assemblyString(int numberOfSymbols, char symbol) {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < numberOfSymbols; i++) {
            string.append(symbol);
        }
        return string.toString();
    }

    private void formatTheString(Integer dividend, Integer divisor, int[] index) {
        int tab = calculator.lengthOfStringArrayConvertedFromCurrentDividend(dividend) + 1 - index[0];

        insertFormattedChars(divisor, index, tab);
        calculator.getResult().replace(1, index[0], dividend.toString());
    }

    void modifyResultToView(Integer dividend, Integer divisor) {
        int[] index = new int[3];
        int j;
        int i;
        for (i = 0, j = 0; i < calculator.getResult().length(); i++) {
            if (calculator.getResult().charAt(i) == '\n') {
                index[j] = i;
                j++;
            }

            if (j == 3) {
                break;
            }
        }
        formatTheString(dividend, divisor, index);
    }

    private void insertFormattedChars(int divisor, int[] index, int tab) {
        Formatter formatter = new Formatter();
        calculator.getResult().insert(index[2], formatter.assemblyString(tab, ' ') +
                "│" + calculator.getQuotient().toString());
        calculator.getResult().insert(index[1], formatter.assemblyString(tab, ' ') +
                "│" + formatter.assemblyString(calculator.getQuotient().length(), '-'));
        calculator.getResult().insert(index[0], "│" + divisor);
    }

}

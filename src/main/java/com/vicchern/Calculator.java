package com.vicchern;

class Calculator {
    private static final StringBuilder result = new StringBuilder();
    private static final StringBuilder quotient = new StringBuilder();
    private static final StringBuilder reminder = new StringBuilder();


    String divisionMethodWithReminder(int dividend, Integer divisor) {
        if (divisor == 0) {
            throw new IllegalArgumentException("Divisor cannot be 0, division by zero");
        }

        if (dividend < divisor) {
            return "" + dividend + "/" + divisor + "=0";
        }

        String[] digits = String.valueOf(dividend).split("");

        for (int iCounterInTheLoop = 0; iCounterInTheLoop < digits.length; iCounterInTheLoop++) {
            reminder.append(digits[iCounterInTheLoop]);

            int reminderNumber = new DivisionResult().receiveReminderNumber(divisor, iCounterInTheLoop);
            if (iCounterInTheLoop == digits.length - 1) {
                result.append(String.format(String.format("%%%ds", iCounterInTheLoop + 2), Integer.toString(reminderNumber))).append("\n");
            }
        }
        new Formatter().modifyResultToView(dividend, divisor);

        return result.toString();
    }

     //   find out length of String Array of current dividend that we will use
    //    for right order screening of integers at the console
    int lengthOfStringArrayConvertedFromCurrentDividend(int i) {
        return (int) Math.log10(i) + 1;
    }

    public StringBuilder getResult() {
        return result;
    }

    public StringBuilder getQuotient() {
        return quotient;
    }

    public StringBuilder getReminder() {
        return reminder;
    }
}

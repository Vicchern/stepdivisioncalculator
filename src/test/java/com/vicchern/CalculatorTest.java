package com.vicchern;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CalculatorTest {
    private Calculator calculator;

    @BeforeEach
    void init() {
        calculator = new Calculator();
    }

    @Test
    void shouldReturnExceptionIfArgumentEqualsTo0() {
        assertThrows(IllegalArgumentException.class, () -> calculator.divisionMethodWithReminder(4, 0));
    }

    @Test
    void shouldReturn0IfDivisorBiggerThanDividend() {
        int dividend = 4;
        int divisor = 7;
        String expected = dividend + "/" + divisor + "=0";
        assertEquals(expected, calculator.divisionMethodWithReminder(dividend, divisor));
    }

    @Test
    void ShouldReturnLengthOfStringArrayConvertedFromCurrentDividend() {
        int currentDividend = 100;
        String[] currentDividendToString = Integer.toString(currentDividend).split("");
        int actual = currentDividendToString.length;
        int expected = 1;

        assertEquals(expected, calculator.lengthOfStringArrayConvertedFromCurrentDividend(actual));
    }

    @Test
    void ShouldReturnRightString() {
        String expected = "_78945│4\n" +
                " 4    │-----\n" +
                " -    │19736\n" +
                "_38\n" +
                " 36\n" +
                " --\n" +
                " _29\n" +
                "  28\n" +
                "  --\n" +
                "  _14\n" +
                "   12\n" +
                "   --\n" +
                "   _25\n" +
                "    24\n" +
                "    --\n"   +
                "     1\n";
        assertEquals(expected, calculator.divisionMethodWithReminder(78945, 4));
    }
}

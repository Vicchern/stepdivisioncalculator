package com.vicchern;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DivisionResultTest {
    private DivisionResult divisionResult;

    @BeforeEach
    void init() {
        divisionResult = new DivisionResult();
    }

    @Test
    void shouldReturnReminderWithRightAmountOfSpacesInTheString() {
        String reminder = "7";
        int counterOfTabs = 3;
        String expected = "   _7";
        int multiplyResult = 6;

        assertEquals(expected, divisionResult.receiveReminderWithRightAmountOfSpacesInTheString(counterOfTabs, reminder, multiplyResult));
    }

}
